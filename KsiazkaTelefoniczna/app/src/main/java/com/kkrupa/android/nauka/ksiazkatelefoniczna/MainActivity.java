package com.kkrupa.android.nauka.ksiazkatelefoniczna;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText imie = null;
    EditText nazwisko = null;
    EditText numerTel = null;
    EditText email = null;
    ListView lista = null;
    Button deleteButton = null;
    Button editButton = null;
    ArrayList<Osoba> osoby = new ArrayList<Osoba>();
    Adapter4Osoba adapter = null;
    String imieText = null;
    String nazwiskoText = null;
    String numerTelText = null;
    String emailText = null;
    KontaktyOpenHelper db;
    Toast toast;

    Osoba selectedObjectOnList = null;

    public void onSave(View v) {
        Osoba osoba = getObjectFromEditor();
        //adapter.add(osoba);
        reloadAllData();

//        db.insertContact(imieText, nazwiskoText, numerTelText, emailText);
        if (db.insertContact(osoba)) {
            toast = Toast.makeText(this, "Dodano nowy wpis.", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            toast = Toast.makeText(this, "Wystąpił błąd, spróbuj ponownie.", Toast.LENGTH_SHORT);
            toast.show();
        };
    }

    public void onDelete(View v) {
        db.deleteContact(selectedObjectOnList.getId());
        adapter.remove(selectedObjectOnList);

        selectedObjectOnList = null;
        deleteButton.setEnabled(false);
        editButton.setEnabled(false);
        reloadAllData();

        toast = Toast.makeText(this, "Usunięto osobę.", Toast.LENGTH_SHORT);
        toast.show();
    }

    public void onEdit(View v) {
        getObjectFromEditor(selectedObjectOnList);
        db.updateContact(selectedObjectOnList);
        reloadAllData();

        toast = Toast.makeText(this, "Zapisano zmiany.", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new KontaktyOpenHelper(this);

        imie = (EditText) findViewById(R.id.idImie);
        nazwisko = (EditText) findViewById(R.id.idNazwisko);
        numerTel = (EditText) findViewById(R.id.idNrTel);
        lista = (ListView) findViewById(R.id.idLista);
        email = (EditText) findViewById(R.id.idEmail);

        deleteButton = (Button) findViewById(R.id.idDelete);
        deleteButton.setEnabled(false);
        editButton = (Button) findViewById(R.id.idEdit);
        editButton.setEnabled(false);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                deleteButton.setEnabled(true);
                editButton.setEnabled(true);
                selectedObjectOnList = (Osoba) parent.getItemAtPosition(position);
                displayItemToEdit(selectedObjectOnList);
            }
        });
        adapter = new Adapter4Osoba(this, osoby);
        lista.setAdapter(adapter);

reloadAllData();
    }

    private void displayItemToEdit(Osoba osoba) {
        imie.setText(osoba.getImie());
        nazwisko.setText(osoba.getNazwisko());
        email.setText(osoba.getEmail());
        numerTel.setText(osoba.getNumerTelefonu());
    }

    private Osoba getObjectFromEditor() {
        imieText = imie.getText().toString();
        nazwiskoText = nazwisko.getText().toString();
        numerTelText = numerTel.getText().toString();
        emailText = email.getText().toString();

        return new Osoba(imieText, nazwiskoText, numerTelText, emailText);
    }

    private void getObjectFromEditor(Osoba toFill) {
        imieText = imie.getText().toString();
        nazwiskoText = nazwisko.getText().toString();
        numerTelText = numerTel.getText().toString();
        emailText = email.getText().toString();

        toFill.setImie(imieText);
        toFill.setNazwisko(nazwiskoText);
        toFill.setNumerTelefonu(numerTelText);
        toFill.setEmail(emailText);
    }

    private void reloadAllData(){
        // to refresh only a list:
        // lista.invalidateViews();

        adapter.clear();
        lista.invalidateViews();

        // load existing items
        AsyncTask<Void, Void, List<Osoba>> asyncTask = new AsyncTask<Void, Void, List<Osoba>>() {
            @Override
            protected List<Osoba> doInBackground(Void... params) {
                return db.getAll();
            }

            @Override
            protected void onPostExecute(List<Osoba> osoby) {
                adapter.addAll(osoby);
            }
        };
        asyncTask.execute();
    }

}
