package com.kkrupa.android.nauka.ksiazkatelefoniczna;

public class OsobaVersion2 extends Osoba {

    private int ulicaNumer;
    private String kodMiasta;

    public OsobaVersion2(String imie, String nazwisko, String numerTelefonu, String email, int ulicaNumer, String kodMiasta) {
        this(-1, imie, nazwisko, numerTelefonu, email, ulicaNumer, kodMiasta);
    }

    public OsobaVersion2(int id, String imie, String nazwisko, String numerTelefonu, String email, int ulicaNumer, String kodMiasta) {
        super(id, imie, nazwisko, numerTelefonu, email);
        this.ulicaNumer = ulicaNumer;
        this.kodMiasta = kodMiasta;
    }


    public String getKodMiasta() {
        return kodMiasta;
    }

    public int getUlicaNumer() {
        return ulicaNumer;
    }
}
