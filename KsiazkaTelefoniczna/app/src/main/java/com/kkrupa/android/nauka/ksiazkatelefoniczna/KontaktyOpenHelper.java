package com.kkrupa.android.nauka.ksiazkatelefoniczna;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kkrupa on 21.01.16.
 */
public class KontaktyOpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "kontakty.db";
    private static final String OSOBY_TABLE_NAME = "osoby";
    private static final String ID = "id";
    private static final String IMIE = "imie";
    private static final String NAZWISKO = "nazwisko";
    private static final String NUMERTEL = "numer_telefonu";
    private static final String EMAIL = "email";

    // columns added in the version 2
    private static final String ULICA_NR = "ulica_nr";
    private static final String KOD_MIASTA = "kod_miasto";

    private static final String KONTAKTY_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " + OSOBY_TABLE_NAME +
            " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + IMIE + " TEXT, " + NAZWISKO +
            " TEXT, " + NUMERTEL + " TEXT, " + EMAIL + " TEXT);";

    private static final List<String> KONTAKTY_TABLE_CHANGES_VERSION_2 = Arrays.asList(
            "ALTER TABLE " + OSOBY_TABLE_NAME + " ADD COLUMN " + ULICA_NR + " INTEGER",
            "ALTER TABLE " + OSOBY_TABLE_NAME + " ADD COLUMN " + KOD_MIASTA + " TEXT"
    );

    KontaktyOpenHelper(Context context) {
        // Change  the last parameter in order to trigger the 'onUpgrade' method.
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Called when the database is created for the first time (when the app is installed).
        updateDatabaseToVersion1(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Called when the database needs to be upgraded

        // If for instance you need to upgrade a database from the version 1 directly to the version 5
        // then you also need changes for versions 2,3 and 4 before applying changes for the version 5.
        // In order to do that the 'oldVersion' number is checked.
        switch (oldVersion) {
            case 1: {
                updateDatabaseToVersion1(db);
                // no break here! All the further changes will be applied too!
            }
            case 2: { // it's the newest version
                updateDatabaseToVersion2(db);
                // if the newVersion isn't 2 then do not break the switch. Apply further changes...
                if (newVersion == 2) {
                    break;
                }

            }
        }
    }

    public List<Osoba> getAll() {
        final List<Osoba> result = new ArrayList<>();

        final SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from  " + OSOBY_TABLE_NAME, null);
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                int id = cursor.getInt(cursor.getColumnIndex(ID));
                String name = cursor.getString(cursor.getColumnIndex(IMIE));
                String surname = cursor.getString(cursor.getColumnIndex(NAZWISKO));
                String phone = cursor.getString(cursor.getColumnIndex(NUMERTEL));
                String email = cursor.getString(cursor.getColumnIndex(EMAIL));

                // changes for the Version 2
                if (cursor.getColumnIndex(ULICA_NR) > 0 && cursor.getColumnIndex(KOD_MIASTA) > 0) {
                    int ulicaNr = cursor.getInt(cursor.getColumnIndex(ULICA_NR));
                    String kodMiasta = cursor.getString(cursor.getColumnIndex(KOD_MIASTA));
                    result.add(new OsobaVersion2(id, name, surname, phone, email, ulicaNr, kodMiasta));
                } else {
                    result.add(new Osoba(id, name, surname, phone, email));
                }
                cursor.moveToNext();
            }
        }

        return result;
    }


    public boolean insertContact(Osoba osoba) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMIE, osoba.getImie());
        contentValues.put(NAZWISKO, osoba.getNazwisko());
        contentValues.put(NUMERTEL, osoba.getNumerTelefonu());
        contentValues.put(EMAIL, osoba.getEmail());

        //IF A DATABASE HAS BEEN UPGRADED THEN CREATE NEW OBJECTS, JUST TO SHOW THAT IT WORKS...
        if (DATABASE_VERSION > 1) {
            // THESE FIELDS REFER TO NEW COLUMNS IN A DATABASE.
            // THESE COLUMNS ARE AVAILABLE ONLY IN THE DB VERSION 2.
            contentValues.put(ULICA_NR, 123);
            contentValues.put(KOD_MIASTA, "00-000");
        }
        db.insert(OSOBY_TABLE_NAME, null, contentValues);
        return true;
    }

    public Integer deleteContact(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(OSOBY_TABLE_NAME, "ID = ?", new String[]{Integer.toString(id)});
    }

    public boolean updateContact(Osoba osoba) {
        if (osoba.getId() == -1) {
            return false;
        }

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMIE, osoba.getImie());
        contentValues.put(NAZWISKO, osoba.getNazwisko());
        contentValues.put(NUMERTEL, osoba.getNumerTelefonu());
        contentValues.put(EMAIL, osoba.getEmail());
        db.update(OSOBY_TABLE_NAME, contentValues, "ID = ?", new String[]{Integer.toString(osoba.getId())});
        return true;
    }

    private void updateDatabaseToVersion1(SQLiteDatabase db) {
        db.execSQL(KONTAKTY_TABLE_CREATE);
    }

    private void updateDatabaseToVersion2(SQLiteDatabase db) {
        // apply changes for the 'version 2'.
        for (String change : KONTAKTY_TABLE_CHANGES_VERSION_2) {
            db.execSQL(change);
        }
    }

}
